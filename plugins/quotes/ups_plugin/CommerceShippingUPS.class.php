<?php

class CommerceShippingUPS extends CommerceShippingQuote {
  /**
   * Settings form to configure UPS quoting service
   */
  public function settings_form(&$form, $rules_settings) {
    $form['store-info'] = array(
      '#title' => 'Store information',
      '#type' => 'fieldset'
    );

    $form['store-info']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['name']) ? $rules_settings['store-info']['name'] : '',
    );

    $form['store-info']['owner'] = array(
      '#type' => 'textfield',
      '#title' => t('Owner'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['owner']) ? $rules_settings['store-info']['owner'] : '',
    );

    $form['store-info']['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email Address'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['email']) ? $rules_settings['store-info']['email'] : '',
    );

    $form['store-info']['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['phone']) ? $rules_settings['store-info']['phone'] : '',
    );

    $form['store-info']['fax'] = array(
      '#type' => 'textfield',
      '#title' => t('Fax'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['fax']) ? $rules_settings['store-info']['fax'] : '',
    );

    $form['store-info']['street1'] = array(
      '#type' => 'textfield',
      '#title' => t('Street #1'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['street1']) ? $rules_settings['store-info']['street1'] : '',
    );

    $form['store-info']['street2'] = array(
      '#type' => 'textfield',
      '#title' => t('Street #2'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['street2']) ? $rules_settings['store-info']['street2'] : '',
    );

    $form['store-info']['city'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['city']) ? $rules_settings['store-info']['city'] : '',
    );

    $form['store-info']['zone'] = array(
      '#type' => 'textfield',
      '#title' => t('State/Province'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['zone']) ? $rules_settings['store-info']['zone'] : '',
    );

    $form['store-info']['postal_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Postal Code'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['postal_code']) ? $rules_settings['store-info']['postal_code'] : '',
    );

    $form['store-info']['country'] = array(
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['store-info']['country']) ? $rules_settings['store-info']['country'] : 'US',
    );

    $form['shipment-settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Shipment Settings',
    );

    $form['shipment-settings']['ups_services'] = array(
      '#type' => 'checkboxes',
      '#title' => t('UPS Services'),
      '#description' => t('Select the UPS services that are available to customers.'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_services']) ? $rules_settings['shipment-settings']['ups_services'] : array(),
      '#options' => _commerce_shipping_ups_service_list(),
    );

    $form['shipment-settings']['ups_all_in_one'] = array(
      '#type' => 'radios',
      '#options' => array(
        0 => t('Each in its own package'),
        1 => t('All in one'),
      ),
      '#title' => t('Product packages'),
      '#description' => t('Indicate whether each product is quoted as shipping separately or all in one package. Orders with one kind of product will still use the package quantity to determine the number of packages needed, however.'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_all_in_one']) ? $rules_settings['shipment-settings']['ups_all_in_one'] : 1,
      '#element_validate' => array('rules_ui_element_integer_validate'),
    );

    $form['shipment-settings']['ups_insurance'] = array(
      '#type' => 'checkbox',
      '#title' => t('Package insurance'),
      '#description' => t('When enabled, products are insured for their full value.'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_insurance']) ? $rules_settings['shipment-settings']['ups_insurance'] : 1,
      '#element_validate' => array('rules_ui_element_integer_validate'),
    );

    $form['shipment-settings']['ups_pickup_type'] = array(
      '#type' => 'select',
      '#title' => t('Pickup Type'),
      '#options' => array(
        '01' => 'Daily Pickup',
        '03' => 'Customer Counter',
        '06' => 'One Time Pickup',
        '07' => 'On Call Air',
        '11' => 'Suggested Retail Rates',
        '19' => 'Letter Center',
        '20' => 'Air Service Center',
      ),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_pickup_type']) ? $rules_settings['shipment-settings']['ups_pickup_type'] : '01',
    );

    $form['shipment-settings']['ups_classification'] = array(
      '#title' => t('UPS Customer classification'),
      '#options' => array(
        '01' => t('Wholesale'),
        '03' => t('Occasional'),
        '04' => t('Retail'),
      ),
      '#type' => 'select',
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_classification']) ? $rules_settings['shipment-settings']['ups_classification'] : '04',
      '#description' => t('The kind of customer you are to UPS. For daily pickups the default is wholesale; for customer counter pickups the default is retail; for other pickups the default is occasional.'),
    );
    $form['shipment-settings']['ups_residential_quotes'] = array(
      '#type' => 'radios',
      '#title' => t('Assume UPS shipping quotes will be delivered to'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_residential_quotes']) ? $rules_settings['shipment-settings']['ups_residential_quotes'] : 0,
      '#options' => array(
        0 => t('Business locations'),
        1 => t('Residential locations (extra fees)'),
      ),
    );
    $form['shipment-settings']['ups_negotiated_rates'] = array(
      '#type' => 'radios',
      '#title' => t('Negotiated rates'),
      '#options' => array(1 => t('Yes'), 0 => t('No')),
      '#description' => t('Is your UPS account receiving negotiated rates on shipments?'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_negotiated_rates']) ? $rules_settings['shipment-settings']['ups_negotiated_rates'] : '0',
    );

    $form['shipment-settings']['ups-markup-type'] = array(
      '#type' => 'select',
      '#title' => t('Markup type'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups-markup-type']) ? $rules_settings['shipment-settings']['ups-markup-type'] : 'percentage',
      '#options' => array(
        'percentage' => t('Percentage (%)'),
        'multiplier' => t('Multiplier (×)'),
        'currency' => t('Addition ($)'),
      ),
    );

    $form['shipment-settings']['ups-markup'] = array(
      '#type' => 'textfield',
      '#title' => t('Shipping rate markup'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups-markup']) ? $rules_settings['shipment-settings']['ups-markup'] : '0',
      '#description' => t('Markup shipping rate quote by currency amount, percentage, or multiplier.'),
    );

    $form['shipment-settings']['currency_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Currency Code'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['currency_code']) ? $rules_settings['shipment-settings']['currency_code'] : 'USD',
    );

    $form['shipment-settings']['ups_unit_system'] = array(
      '#type' => 'textfield',
      '#title' => t('Unit of Measure'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['shipment-settings']['ups_unit_system']) ? $rules_settings['shipment-settings']['ups_unit_system'] : 'in',
    );

    $form['connection-settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'UPS Connection Settings',
    );

    $form['connection-settings']['ups_connection_address'] = array(
      '#type' => 'textfield',
      '#title' => t('Connection Address'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['ups_connection_address']) ? $rules_settings['connection-settings']['ups_connection_address'] : 'https://wwwcie.ups.com/ups.app/xml/',
    );

    $form['connection-settings']['ups_shipper_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Shipper #'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['ups_shipper_number']) ? $rules_settings['connection-settings']['ups_shipper_number'] : '',
      '#description' => t('The 6-character string identifying your UPS account as a shipper.'),
    );

    $form['connection-settings']['ups_access_license'] = array(
      '#type' => 'textfield',
      '#title' => t('UPS OnLine Tools XML Access Key'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['ups_access_license']) ? $rules_settings['connection-settings']['ups_access_license'] : '',
    );

    $form['connection-settings']['ups_user_id'] = array(
      '#type' => 'textfield',
      '#title' => t('UPS.com user ID'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['ups_user_id']) ? $rules_settings['connection-settings']['ups_user_id'] : '',
    );

    $form['connection-settings']['ups_password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => is_array($rules_settings) && isset($rules_settings['connection-settings']['ups_password']) ? $rules_settings['connection-settings']['ups_password'] : '',
    );

  }

  /**
   * Checkout Form for selecting UPS shipment method
   */
  public function submit_form($pane_values, $checkout_pane, $order = NULL) {
    if (empty($order)) {
      $order = $this->order;
    }
    $form = parent::submit_form($pane_values, $checkout_pane, $order);

    // Merge in values from the order.
    if (!empty($order->data['commerce_shipping_ups'])) {
      $pane_values += $order->data['commerce_shipping_ups'];
    }

    // Merge in default values.
    $pane_values += array(
      'method' => '03',
    );

    /* TODO: Find a better way with some nice array function */
    $all_methods = _commerce_shipping_ups_service_list();
    $methods = array();
    foreach ($this->settings['shipment-settings']['ups_services'] as $key => $service) {
      if ($service !== 0) {
        $methods[$key] = $all_methods[$key];
      }
    }

    $form['method'] = array(
      '#type' => 'radios',
      '#title' => t('Shipment Method'),
      '#default_value' => '03',
      '#options' => $methods,
    );

    return $form;
  }

  /**
   * Validation form.
   */
  public function submit_form_validate($pane_form, $pane_values, $form_parents = array(), $order = NULL) {
  }

  /**
   * Calculate Quote
   */
  public function calculate_quote($currency_code, $form_values = array(), $order = NULL, $pane_form = NULL, $pane_values = NULL) {
    $method = $form_values['method'];
    $shipping_address = $pane_values['values']['customer_profile_shipping']['commerce_customer_address'][LANGUAGE_NONE][0];
    $quotes = $this->ups_quote($order, $method, $shipping_address);

    $all_methods = _commerce_shipping_ups_service_list();

    if (empty($order)) {
      $order = $this->order;
    }
    $settings = $this->settings;
    $shipping_line_items = array();
    $shipping_line_items[] = array(
      'amount' => commerce_currency_decimal_to_amount($quotes[$method]['rate'], $currency_code),
      'currency_code' => $currency_code,
      'label' => t($all_methods[$method]),
    );

    return $shipping_line_items;
  }

  /**
   * Callback for retrieving a UPS shipping quote.
   *
   * Request a quote for each enabled UPS Service. Therefore, the quote will
   * take longer to display to the user for each option the customer has available.
   *
   * @param $order
   *   Cart Order.
   *
   * @param $method
   *   The Shipping Method to Quote For
   *
   * @param $shipping_address
   *   The Address to Ship to
   *
   * @return
   *   JSON object containing rate, error, and debugging information.
   */
  private function ups_quote($order, $method, $shipping_address) {
    $last_key = 0;
    $store_info = $this->settings['store-info'];
    $shipment_settings = $this->settings['shipment-settings'];
    $connection_settings = $this->settings['connection-settings'];

    $quotes = array();
    $quotes[$method] = array();

    // TODO: Support Multiple Addresses
    $addresses = array($store_info);

    $packages = array();
    // TODO: Figure out what the count() call in ubercart was trying to do
    if ($shipment_settings['ups_all_in_one']) { // && count($order->commerce_line_items[LANGUAGE_NONE]) > 1) {
      foreach ($order->commerce_line_items[LANGUAGE_NONE] as $order_line) {
        $line_item_id = $order_line['line_item_id'];
        $line_item = commerce_line_item_load($line_item_id);
        $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);

        $width = isset($product->field_width[LANGUAGE_NONE]) ? $product->field_width[LANGUAGE_NONE][0]['value'] : 0;
        $height = isset($product->field_height[LANGUAGE_NONE]) ? $product->field_height[LANGUAGE_NONE][0]['value'] : 0;
        $length = isset($product->field_length[LANGUAGE_NONE]) ? $product->field_length[LANGUAGE_NONE][0]['value'] : 0;
        $product_weight = isset($product->field_weight[LANGUAGE_NONE]) ? $product->field_weight[LANGUAGE_NONE][0]['value'] : 0;

        if ($product->product_id) {
          // Packages are grouped by the address from which they will be
          // shipped. We will keep track of the different addresses in an array
          // and use their keys for the array of packages.

          // TODO: Support Multiple Addresses
          // $address = (array)uc_quote_get_default_shipping_address($product->nid);
          $address = $addresses[0];
          $key = array_search($address, $addresses);
          if ($key === FALSE) {
            // This is a new address. Increment the address counter $last_key
            // instead of using [] so that it can be used in $packages and
            // $addresses.
            $addresses[++$last_key] = $address;
            $key = $last_key;
          }
        }

        // Add this product to the last package from the found address or start
        // a new package.
        if (isset($packages[$key]) && count($packages[$key])) {
          $package = array_pop($packages[$key]);
        }
        else {
          $package = _commerce_shipping_ups_new_package();
        }

        $weight = $product_weight * $line_item->quantity;

        $package->weight += $weight;
        $package->price = $product->commerce_price[LANGUAGE_NONE][0]['amount'] * $line_item->quantity;

        $conversion = 1;
        $package->length = max($length * $conversion, $package->length);
        $package->width = max($width * $conversion, $package->width);
        $package->height = max($height * $conversion, $package->height);

        $packages[$key][] = $package;
      }

      foreach ($packages as $addr_key => $shipment) {
        foreach ($shipment as $key => $package) {
          if (!$package->weight) {
            unset($packages[$addr_key][$key]);
            continue;
          }
          elseif ($package->weight > 150) {
            // UPS has a weight limit on packages of 150 lbs. Pretend the
            // products can be divided into enough packages.
            $qty = floor($package->weight / 150) + 1;
            $package->qty = $qty;
            $package->weight /= $qty;
            $package->price /= $qty;
          }
        }
      }
    } else {
      foreach ($order->commerce_line_items[LANGUAGE_NONE] as $order_line) {

        $line_item_id = $order_line['line_item_id'];

        $line_item = commerce_line_item_load($line_item_id);
        $product = commerce_product_load($line_item->commerce_product[LANGUAGE_NONE][0]['product_id']);

        $width = isset($product->field_width[LANGUAGE_NONE]) ? $product->field_width[LANGUAGE_NONE][0]['value'] : 0;
        $height = isset($product->field_height[LANGUAGE_NONE]) ? $product->field_height[LANGUAGE_NONE][0]['value'] : 0;
        $length = isset($product->field_length[LANGUAGE_NONE]) ? $product->field_length[LANGUAGE_NONE][0]['value'] : 0;
        $weight = isset($product->field_weight[LANGUAGE_NONE]) ? $product->field_weight[LANGUAGE_NONE][0]['value'] : 0;

        $key = 0;
        if ($product->product_id) {
          // TODO: Support Multiple Addresses
          // $address = (array)uc_quote_get_default_shipping_address($product->nid);
          $address = $addresses[0];

          $key = array_search($address, $addresses);
          if ($key === FALSE) {
            $addresses[++$last_key] = $address;
            $key = $last_key;
          }
        }
        if (!isset($product->pkg_qty) || !$product->pkg_qty)  {
          $product->pkg_qty = 1;
        }
        $num_of_pkgs = (int)($line_item->quantity / $product->pkg_qty);
        if ($num_of_pkgs) {
          $package = _commerce_shipping_ups_new_package();
          //$package = drupal_clone($product);
          $package->description = $product->sku;
          $package->weight = $weight * $product->pkg_qty;
          $package->width = $width * $product->pkg_qty;
          $package->height = $height * $product->pkg_qty;
          $package->length = $length * $product->pkg_qty;
          $package->price = $product->commerce_price[LANGUAGE_NONE][0]['amount'] * $product->pkg_qty;
          $package->qty = $num_of_pkgs;
          $package->pkg_type = isset($product->ups) ? $product->ups['pkg_type'] : '02';
          if ($package->weight) {
            $packages[$key][] = $package;
          }
        }

        // TODO: Package Quantity
        $remaining_qty = $line_item->quantity % $product->pkg_qty;
        if ($remaining_qty) {
          $package = drupal_clone($product);
          $package->description = $product->model;
          $package->weight = $product->weight * $remaining_qty;
          $package->price = $product->price * $remaining_qty;
          $package->qty = 1;
          $package->pkg_type = $product->ups ? $product->ups['pkg_type'] : '02';
          if ($package->weight) {
            $packages[$key][] = $package;
          }
        }
      }
    }
    if (!count($packages)) {
      return array();
    }

    $dest = (object)$shipping_address;

    foreach ($packages as $key => $ship_packages) {
      $orig = (object)$addresses[$key];
      $orig->email = $store_info['email'];

      $request = $this->ups_shipping_quote_request($ship_packages, $orig, $dest, $method);
      $resp = drupal_http_request($this->settings['connection-settings']['ups_connection_address'] .'Rate', array('method' => 'POST', 'data' => $request));

      // TODO: Implement Debugging
      // if (user_access('configure quotes') && variable_get('uc_quote_display_debug', FALSE)) {
      //  $quotes[$ups_service]['debug'] .= /* '<pre>'. print_r($ship_packages, TRUE) .'</pre>' . */ htmlentities($request) .' <br /><br /> '. htmlentities($resp->data);
      // }

      $response = new SimpleXMLElement($resp->data);
      if (isset($response->Response->Error)) {
        foreach ($response->Response->Error as $error) {
          // TODO: Implement Debugging
          // if (user_access('configure quotes') && variable_get('uc_quote_display_debug', FALSE)) {
          //   $quotes[$ups_service]['error'][] = (string)$error->ErrorSeverity .' '. (string)$error->ErrorCode .': '. (string)$error->ErrorDescription;
          // }
          if (strpos((string)$error->ErrorSeverity, 'Hard') !== FALSE) {
            // All or nothing quote. If some products can't be shipped by
            // a certain service, no quote is given for that service. If
            // that means no quotes are given at all, they'd better call in.
            unset($quotes[$method]['rate']);
          }
        }
      }
      // if NegotiatedRates exist, quote based on those, otherwise, use TotalCharges
      if (isset($response->RatedShipment)) {
        $charge = $response->RatedShipment->TotalCharges;
        if (isset($response->RatedShipment->NegotiatedRates)) {
          $charge = $response->RatedShipment->NegotiatedRates->NetSummaryCharges->GrandTotal;
        }
        if (!isset($charge->CurrencyCode) || (string)$charge->CurrencyCode == $this->settings['shipment-settings']['currency_code']) {
          $rate = $this->ups_markup((string)$charge->MonetaryValue);
          $quotes[$method]['rate'] += $rate;
        }
      }
    }
    // uasort($quotes, 'uc_quote_price_sort');
    $context = array(
      'revision' => 'themed',
      'type' => 'amount',
    );
    foreach ($quotes as $key => $quote) {
      if (isset($quote['rate'])) {
        $context['subject']['quote'] = $quote;
        $context['revision'] = 'altered';
        $quotes[$key]['rate'] = $quote['rate'];
        $context['revision'] = 'formatted';
        $quotes[$key]['format'] = $quote['rate'];
        //$quotes[$key]['option_label'] = theme('commerce_shipping_ups_option_label', $method['ups']['quote']['accessorials'][$key]);
      }
    }

    /**
     * Ugly hack to work around PHP bug, details here:
     *   http://bugs.php.net/bug.php?id=23220
     * We strip out errors that look something like:
     *  warning: fread() [function.fread]: SSL fatal protocol error in...
     * Copied from http://drupal.org/node/70915 and then improved by Lyle.
     */
    $messages = drupal_set_message();
    $errors = $messages['error'];
    $total = count($errors);
    for ($i = 0; $i <= $total; $i++) {
      if (strpos($errors[$i], 'SSL: fatal protocol error in')) {
        unset($_SESSION['messages']['error'][$i]);
      }
    }
    if (empty($_SESSION['messages']['error'])) {
      unset($_SESSION['messages']['error']);
    }
    db_query("DELETE FROM {watchdog} WHERE type = 'php' AND variables LIKE '%%SSL: fatal protocol error%%'");
    // End of ugly hack.

    return $quotes;
  }

  /**
   * Construct an XML quote request.
   *
   * @param $packages
   *   Array of packages received from the cart.
   * @param $origin
   *   Delivery origin address information.
   * @param $destination
   *   Delivery destination address information.
   * @param $ups_service
   *   UPS service code (refers to UPS Ground, Next-Day Air, etc.).
   * @return
   *   RatingServiceSelectionRequest XML document to send to UPS
   */
  private function ups_shipping_quote_request($packages, $origin, $destination, $ups_service) {
    $store = $this->settings['store-info'];

    $account = $this->settings['connection-settings']['ups_shipper_number'];
    $ua = explode(' ', $_SERVER['HTTP_USER_AGENT']);
    $user_agent = $ua[0];

    $services = _commerce_shipping_ups_service_list();
    $service = array('code' => $ups_service, 'description' => $services[$ups_service]);

    $pkg_types = _commerce_shipping_ups_pkg_types();

    $shipper_zone = $store['zone'];
    $shipper_country = $store['country'];
    $shipper_zip = $store['postal_code'];
    $shipto_zone = $destination->administrative_area;
    $shipto_country = $destination->country;
    $shipto_zip = $destination->postal_code;
    $shipfrom_zone = $origin->zone;
    $shipfrom_country = $origin->country;
    $shipfrom_zip = $origin->postal_code;

    $ups_units = $this->settings['shipment-settings']['ups_unit_system'];

    // TODO: Handle other unit types than imperial
    switch ($ups_units) {
      case 'in':
        $units = 'LBS';
        $unit_name = 'Pounds';
        break;
      case 'cm':
        $units = 'KGS';
        $unit_name = 'Kilograms';
        break;
    }

    $shipment_weight = 0;
    $package_schema = '';
    foreach ($packages as $package) {
      $qty = $package->qty;
      for ($i = 0; $i < $qty; $i++) {
        $package_type = array('code' => $package->pkg_type, 'description' => $pkg_types[$package->pkg_type]);
        $package_schema .= "<Package>";
        $package_schema .= "<PackagingType>";
        $package_schema .= "<Code>". $package_type['code'] ."</Code>";
        //$package_schema .= "<Description>". $package_type['description'] ."</Description>";
        $package_schema .= "</PackagingType>";
        if ($package->pkg_type == '02' && $package->length && $package->width && $package->height) {
          if ($package->length < $package->width) {
            list($package->length, $package->width) = array($package->width, $package->length);
          }
          $package_schema .= "<Dimensions>";
          $package_schema .= "<UnitOfMeasurement>";
          // $conversion = uc_length_conversion($package->length_units, variable_get('commerce_shipping_ups_unit_system', variable_get('uc_length_unit', 'in')));
          $conversion = 1;
          $package_schema .= "<Code>". strtoupper($this->settings['shipment-settings']['ups_unit_system']) ."</Code>";
          $package_schema .= "</UnitOfMeasurement>";
          $package_schema .= "<Length>". number_format($package->length * $conversion, 2, '.', '') ."</Length>";
          $package_schema .= "<Width>". number_format($package->width * $conversion, 2, '.', '') ."</Width>";
          $package_schema .= "<Height>". number_format($package->height * $conversion, 2, '.', '') ."</Height>";
          $package_schema .= "</Dimensions>";
        }
        $size = $package->length * $conversion + 2 * $conversion * ($package->width + $package->height);

        // TODO: Handle other unit types than imperial
        switch ($ups_units) {
          case 'in':
            $conversion = 1;
            break;
          case 'cm':
            $conversion = uc_weight_conversion($package->weight_units, 'kg');
            break;
        }
        $weight = max(1, $package->weight * $conversion);
        $shipment_weight += $weight;
        $package_schema .= "<PackageWeight>";
        $package_schema .= "<UnitOfMeasurement>";
        $package_schema .= "<Code>$units</Code>";
        $package_schema .= "<Description>$unit_name</Description>";
        $package_schema .= "</UnitOfMeasurement>";
        $package_schema .= "<Weight>". number_format($weight, 1, '.', '') ."</Weight>";
        $package_schema .= "</PackageWeight>";
        if ($size > 130 && $size <= 165) {
          $package_schema .= "<LargePackageIndicator/>";
        }
        if ($this->settings['shipment-settings']['ups_insurance']) {
          $package_schema .= "<PackageServiceOptions>";
          $package_schema .= "<InsuredValue>";
          $package_schema .= "<CurrencyCode>". $this->settings['shipment-settings']['currency_code'] ."</CurrencyCode>";
          // TODO: Handle Commerce Prices
          $package_schema .= "<MonetaryValue>". $package->price / 100 ."</MonetaryValue>";
          $package_schema .= "</InsuredValue>";
          $package_schema .= "</PackageServiceOptions>";
        }
        $package_schema .= "</Package>";
      }
    }

    $schema = $this->ups_access_request() ."
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<RatingServiceSelectionRequest xml:lang=\"en-US\">
  <Request>
    <TransactionReference>
      <CustomerContext>Complex Rate Request</CustomerContext>
      <XpciVersion>1.0001</XpciVersion>
    </TransactionReference>
    <RequestAction>Rate</RequestAction>
    <RequestOption>rate</RequestOption>
  </Request>
  <PickupType>
    <Code>". $this->settings['shipment-settings']['ups_pickup_type'] ."</Code>
  </PickupType>
  <CustomerClassification>
    <Code>". $this->settings['shipment-settings']['ups_classification'] ."</Code>
  </CustomerClassification>
  <Shipment>
    <Shipper>
      <ShipperNumber>". $this->settings['connection-settings']['ups_shipper_number'] ."</ShipperNumber>
      <Address>
        <City>". $store['city'] ."</City>
        <StateProvinceCode>$shipper_zone</StateProvinceCode>
        <PostalCode>$shipper_zip</PostalCode>
        <CountryCode>$shipper_country</CountryCode>
      </Address>
    </Shipper>
    <ShipTo>
      <Address>
        <StateProvinceCode>$shipto_zone</StateProvinceCode>
        <PostalCode>$shipto_zip</PostalCode>
        <CountryCode>$shipto_country</CountryCode>
      ";
    if ($this->settings['shipment-settings']['ups_residential_quotes']) {
      $schema .= "<ResidentialAddressIndicator/>
      ";
    }
    $schema .= "</Address>
    </ShipTo>
    <ShipFrom>
      <Address>
        <StateProvinceCode>$shipfrom_zone</StateProvinceCode>
        <PostalCode>$shipfrom_zip</PostalCode>
        <CountryCode>$shipfrom_country</CountryCode>
      </Address>
    </ShipFrom>
    <ShipmentWeight>
      <UnitOfMeasurement>
        <Code>$units</Code>
        <Description>$unit_name</Description>
      </UnitOfMeasurement>
      <Weight>". number_format($shipment_weight, 1, '.', '') ."</Weight>
    </ShipmentWeight>
    <Service>
      <Code>{$service['code']}</Code>
      <Description>{$service['description']}</Description>
    </Service>
    ";
    $schema .= $package_schema;
    if ($this->settings['shipment-settings']['ups_negotiated_rates']) {
      $schema .= "<RateInformation>
          <NegotiatedRatesIndicator/>
        </RateInformation>";
    }
    $schema .= "</Shipment>
</RatingServiceSelectionRequest>";

    return $schema;
  }

/**
 * Return XML access request to be prepended to all requests to the UPS webservice.
 */
  private function ups_access_request() {
    $access = $this->settings['connection-settings']['ups_access_license'];
    $user = $this->settings['connection-settings']['ups_user_id'];
    $password = $this->settings['connection-settings']['ups_password'];
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<AccessRequest xml:lang=\"en-US\">
  <AccessLicenseNumber>$access</AccessLicenseNumber>
  <UserId>$user</UserId>
  <Password>$password</Password>
</AccessRequest>
";
  }

  /**
   * Modify the rate received from UPS before displaying to the customer.
   */
  private function ups_markup($rate) {
    $markup = $this->settings['shipment-settings']['ups-markup'];
    $type = $this->settings['shipment-settings']['ups-markup-type'];
    if (is_numeric(trim($markup))) {
      switch ($type) {
        case 'percentage':
          return $rate + $rate * floatval(trim($markup)) / 100;
        case 'multiplier':
          return $rate * floatval(trim($markup));
        case 'currency':
          return $rate + floatval(trim($markup));
      }
    }
    else {
      return $rate;
    }
  }

}

/** Lifted from Ubercart **/



/**
 * Convenience function to get UPS codes for their services.
 */
function _commerce_shipping_ups_service_list() {
  return array(
    '03' => t('UPS Ground'),
    '11' => t('UPS Standard'),
    '01' => t('UPS Next Day Air'),
    '13' => t('UPS Next Day Air Saver'),
    '14' => t('UPS Next Day Early A.M.'),
    '02' => t('UPS 2nd Day Air'),
    '59' => t('UPS 2nd Day Air A.M.'),
    '12' => t('UPS 3-Day Select'),
    '07' => t('UPS Worldwide Express'),
    '08' => t('UPS Worldwide Expedited'),
    '54' => t('UPS Worldwide Express Plus'),
    '65' => t('UPS Saver'),
    '82' => t('UPS Today Standard'),
    '83' => t('UPS Today Dedicated Courrier'),
    '84' => t('UPS Today Intercity'),
    '85' => t('UPS Today Express'),
    '86' => t('UPS Today Express Saver'),
  );
}

/**
 * Convenience function to get UPS codes for their package types.
 */
function _commerce_shipping_ups_pkg_types() {
  return array(
    '01' => t('UPS Letter'),
    '02' => t('Customer Supplied Package'),
    '03' => t('Tube'),
    '04' => t('PAK'),
    '21' => t('UPS Express Box'),
    '24' => t('UPS 25KG Box'),
    '25' => t('UPS 10KG Box'),
    '30' => t('Pallet'),
    '2a' => t('Small Express Box'),
    '2b' => t('Medium Express Box'),
    '2c' => t('Large Express Box'),
  );
}

/**
 * Pseudo-constructor to set default values of a package.
 */
function _commerce_shipping_ups_new_package() {
  $package = new stdClass();

  $package->weight = 0;
  $package->price = 0;

  $package->length = 0;
  $package->width = 0;
  $package->height = 0;

  $package->length_units = 'in';
  $package->weight_units = 'lb';
  $package->qty = 1;
  $package->pkg_type = '02';

  return $package;
}

